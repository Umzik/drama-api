
from rest_framework import generics
from rest_framework.parsers import MultiPartParser, FormParser

from common.gallery.models import Gallery, GalleryItem, GalleryPhoto
from .serializers import GallerySerializer, GalleryItemSerializer, GalleryPhotoSerializer


# List and Create
class GalleryListCreate(generics.ListCreateAPIView):
    queryset = Gallery.objects.all()
    serializer_class = GallerySerializer
    parser_classes = (MultiPartParser, FormParser)

# Retrieve, Update and Delete
class GalleryRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Gallery.objects.all()
    serializer_class = GallerySerializer
    parser_classes = (MultiPartParser, FormParser)

class GalleryItemListCreate(generics.ListCreateAPIView):
    queryset = GalleryItem.objects.all()
    serializer_class = GalleryItemSerializer
    parser_classes = (MultiPartParser, FormParser)

# Retrieve, Update and Delete
class GalleryItemRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = GalleryItem.objects.all()
    serializer_class = GalleryItemSerializer
    parser_classes = (MultiPartParser, FormParser)

class GalleryPhotoListCreate(generics.ListCreateAPIView):
    queryset = GalleryPhoto.objects.all()
    serializer_class = GalleryPhotoSerializer
    parser_classes = (MultiPartParser, FormParser)

# Retrieve, Update and Delete
class GalleryPhotoRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = GalleryPhoto.objects.all()
    serializer_class = GalleryPhotoSerializer
    parser_classes = (MultiPartParser, FormParser)