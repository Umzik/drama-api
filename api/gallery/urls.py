from django.urls import path
from . import views

urlpatterns = [
    path('', views.GalleryListCreate.as_view(), name='gallery-list-create'),
    path('<int:pk>/', views.GalleryRetrieveUpdateDestroy.as_view(), name='gallery-detail'),

    path('-item/', views.GalleryItemListCreate.as_view(), name='gallery-item-list-create'),
    path('-item/<int:pk>/', views.GalleryItemRetrieveUpdateDestroy.as_view(), name='gallery-item-detail'),

    path('-photo/', views.GalleryPhotoListCreate.as_view(), name='gallery-photo-list-create'),
    path('-photo/<int:pk>/', views.GalleryPhotoRetrieveUpdateDestroy.as_view(), name='gallery-photo-detail'),
]