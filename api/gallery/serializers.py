from rest_framework import serializers

from common.gallery.models import Gallery, GalleryItem, GalleryPhoto


class GalleryItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = GalleryItem
        fields = ('gallery', 'video')


class GalleryPhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = GalleryPhoto
        fields = ('gallery', 'image')


class GallerySerializer(serializers.ModelSerializer):
    galleryitems = serializers.SerializerMethodField()
    galleryphotos = serializers.SerializerMethodField()

    class Meta:
        model = Gallery
        fields = ('id', 'title', 'description', 'posted_by', 'galleryitems', 'galleryphotos')

    def get_galleryitems(self, obj):
        items = obj.galleryitem.all()
        return GalleryItemSerializer(items, many=True).data

    def get_galleryphotos(self, obj):
        photos = obj.galleryphoto.all()
        return GalleryPhotoSerializer(photos, many=True).data


