from rest_framework import serializers

from api.timetable.serializers import SessionSerializer
from api.user_app.serializers import CustomUserSerializer
from common.best_actor.models import BestActor


class BestActorListSerializer(serializers.ModelSerializer):

    class Meta:
        model = BestActor
        fields = ['actor', 'session']

class BestActorDetailSerializer(serializers.ModelSerializer):
    actor = CustomUserSerializer()
    session = SessionSerializer()

    class Meta:
        model = BestActor
        fields = ['actor', 'session']

