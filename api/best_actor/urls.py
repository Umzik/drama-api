from django.urls import path
from . import views

urlpatterns = [
    path('-list/', views.BestActorListAPIView.as_view(), name='gallery-list'),
    path('-create/', views.BestActorCreateAPIView.as_view(), name='gallery-create'),
    path('/<int:pk>/', views.BestActorDetailAPIView.as_view(), name='gallery-detail'),
]