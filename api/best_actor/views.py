from django.shortcuts import render
from rest_framework import generics

from api.best_actor.serializers import BestActorListSerializer, BestActorDetailSerializer
from common.best_actor.models import BestActor


# Create your views here.


class BestActorCreateAPIView(generics.CreateAPIView):
    queryset = BestActor.objects.all()
    serializer_class = BestActorListSerializer

class BestActorCreateAPIView(generics.CreateAPIView):
    queryset = BestActor.objects.all()
    serializer_class = BestActorListSerializer


class BestActorListAPIView(generics.ListAPIView):
    queryset = BestActor.objects.all()
    serializer_class = BestActorListSerializer


class BestActorDetailAPIView(generics.ListAPIView):
    queryset = BestActor.objects.all()
    serializer_class = BestActorDetailSerializer



