from os import path

from django.urls import include
from rest_framework.routers import DefaultRouter

from api.announcement.views import AnnouncementViewSet
from api.scripts.views import ScriptViewSet
from api.timetable.views import SessionViewSet

router = DefaultRouter()
router.register(r'session', SessionViewSet)
router.register(r'scripts', ScriptViewSet)
router.register(r'announcement', AnnouncementViewSet)


urlpatterns = router.urls

