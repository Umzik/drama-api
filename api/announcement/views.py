from django.shortcuts import render
from rest_framework import viewsets

from api.announcement.serializers import AnnouncementSerializer
from common.announcement.models import Announcement


class AnnouncementViewSet(viewsets.ModelViewSet):
    queryset = Announcement.objects.all()
    serializer_class = AnnouncementSerializer

