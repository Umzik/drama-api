from django.shortcuts import render
from rest_framework import viewsets

from api.timetable.serializers import SessionSerializer
from common.timetable.models import Session


# Create your views here.
class SessionViewSet(viewsets.ModelViewSet):
    queryset = Session.objects.all()
    serializer_class = SessionSerializer