from django.shortcuts import render
from rest_framework import viewsets

from api.scripts.serializers import ScriptSerializer
from common.scripts.models import Script


class ScriptViewSet(viewsets.ModelViewSet):
    queryset = Script.objects.all()
    serializer_class = ScriptSerializer
