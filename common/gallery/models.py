from django.contrib.auth import get_user_model
from django.db import models

from common.user_app.models import CustomUser

# Create your models here.


class Gallery(models.Model):
    title = models.CharField(max_length=50)
    description = models.TextField()
    posted_by = models.ForeignKey(CustomUser, related_name="usergallery", on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.title

class GalleryItem(models.Model):
    gallery = models.ForeignKey(Gallery, on_delete=models.CASCADE, related_name="galleryitem")
    video = models.FileField(upload_to='videos/', null=True, verbose_name="")

class GalleryPhoto(models.Model):
    gallery = models.ForeignKey(Gallery, on_delete=models.CASCADE, related_name="galleryphoto")
    image = models.ImageField(upload_to='photos/', null=True, verbose_name="")
