from django.contrib import admin
from .models import Gallery, GalleryItem, GalleryPhoto

class GalleryItemInline(admin.TabularInline):
    model = GalleryItem
    extra = 1  # Number of empty forms to display

class GalleryPhotoInline(admin.TabularInline):
    model = GalleryPhoto
    extra = 1

class GalleryAdmin(admin.ModelAdmin):
    inlines = [GalleryItemInline, GalleryPhotoInline]


admin.site.register(Gallery, GalleryAdmin)
