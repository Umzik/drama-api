from django.contrib.auth import get_user_model
from django.db import models

from common.user_app.models import CustomUser


class Script(models.Model):
    version = models.DecimalField(max_digits=3, decimal_places=2)
    file = models.FileField(upload_to="./script_files")
    note = models.TextField()
    posted_by = models.ForeignKey(CustomUser, related_name="userscript", on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.version
