from django.db import models

from common.timetable.models import Session
from common.user_app.models import CustomUser


# Create your models here.

class BestActor(models.Model):
    actor = models.OneToOneField(CustomUser, on_delete=models.CASCADE, related_name="bestactor")
    session = models.OneToOneField(Session, on_delete=models.CASCADE, related_name="bestactorsession")

    def __str__(self):
        return self.session
