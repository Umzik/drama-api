from django.contrib import admin

from common.best_actor.models import BestActor

# Register your models here.
admin.site.register(BestActor)