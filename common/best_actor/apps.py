from django.apps import AppConfig


class BestActorConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'common.best_actor'
