from django.db import models
from django.contrib.auth import get_user_model
from django.db import models

from common.user_app.models import CustomUser

# Create your models here.


class Announcement(models.Model):
    title = models.CharField(max_length=200)
    body = models.TextField()
    date_posted = models.DateTimeField(auto_now_add=True)
    posted_by = models.ForeignKey(CustomUser, related_name="userannouncement", on_delete=models.SET_NULL, null=True)
    img = models.ImageField()


    def __str__(self):
        return self.title
