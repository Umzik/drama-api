from time import strftime

from django.contrib.auth import get_user_model
from django.db import models

from common.user_app.models import CustomUser



class Session(models.Model):
    title = models.CharField(max_length=50)
    date = models.DateField()
    time = models.TimeField()
    venue = models.CharField(max_length=30)
    desc = models.TextField()
    posted_by = models.ForeignKey(CustomUser, related_name="usertimetable", on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.title
