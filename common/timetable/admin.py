from django.contrib import admin

from common.timetable.models import Session

# Register your models here.
admin.site.register(Session)